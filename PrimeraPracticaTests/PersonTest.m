//
//  PersonTest.m
//  PrimeraPractica
//
//  Created by ULPGC on 7/12/16.
//  Copyright © 2016 ULPGC. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Persona.h"

@interface PersonTest : XCTestCase

-(void)testNombreCompleto1;
-(void)testNombreCompleto2;

@end

@implementation PersonTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    
}

// Este test comprueba el funcionamiento normal del metodo nombreCompleto comparando un string nombre y apellido
// con el obtenido mediante el empleo de dicho metodo
-(void)testNombreCompleto1{
    Persona* persona1= [[Persona alloc] init];
    persona1.nombre=@"Javi ";
    persona1.apellido=@"Varas";
    
    NSString *nombre=@"Javi ";
    NSString *apellido=@"Varas";
    
    NSString *nombreApellido =[nombre stringByAppendingString: apellido];
    NSString *prueba= [persona1 nombreCompleto: persona1];
    
    if ([prueba isEqualToString: nombreApellido]) {
        NSLog(@"El test NombreCompleto1 se ha realizado correctamente");
    }else{
        NSLog(@"El test NombreCompleto1 no se ha realizado correctamente");
    }
}
// Este test comprueba el funcionamiento del metodo nombreCompleto cuando a traves del metodo no se introducen los datos
// de la persona
-(void)testNombreCompleto2{
    Persona* persona1= [[Persona alloc] init];
    NSString *nombre=@"Javi";
    NSString *apellido=@"Varas";
    
    
    NSString *nombreApellido =[nombre stringByAppendingString: apellido];
    NSString *prueba= [persona1 nombreCompleto: persona1];
    
    if ([prueba isEqualToString: nombreApellido]) {
        NSLog(@"El test NombreCompleto2 no se ha realizado correctamente");
    }else{
        NSLog(@"El test NombreCompleto2 se ha realizado correctamente");
    }
}

-(void)testBuscar1{
    Persona* persona1= [[Persona alloc] init];
    persona1.nombre=@"David";
    
    if ([persona1 buscar: persona1]) {
        NSLog(@"El test Buscar1 se ha realizado correctamente");
    }else{
        NSLog(@"El test Buscar1 no se ha realizado correctamente");
    }
}

-(void)testBuscar2{
    Persona* persona1= [[Persona alloc] init];
    
    if ([persona1 buscar: persona1]) {
        NSLog(@"El test Buscar2 no se ha realizado correctamente");
    }else{
        NSLog(@"El test Buscar2 se ha realizado correctamente");
    }
}
    


//- (void)testPerformanceExample {
    // This is an example of a performance test case.
//    [self measureBlock:^{
        // Put the code you want to measure the time of here.
//    }];
//}

@end
