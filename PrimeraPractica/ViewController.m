//
//  ViewController.m
//  PrimeraPractica
//
//  Created by ULPGC on 29/11/16.
//  Copyright © 2016 ULPGC. All rights reserved.
//

#import "ViewController.h"
#import "Persona.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Ejercicio 1
    int pos=0;
    NSLog(@"hola mundo");
    NSString *miString=@"Alejandro";
    NSNumber *miNumero=@5;
    NSArray  *miArray= @[miString, miNumero];
    NSDictionary *miD= @ {miString:miNumero};
    
    NSLog(@"El valor del string es %@", miString);
    
    NSLog(@"El valor de la posicion %d es %@",pos, miArray[pos]);
    
    NSLog(@"El valor de la clave %@ es %@",miString, miD[miString]);
    
    
    //Ejercicio 2.4
    
    // objetos de la clase Persona
    Persona* persona1= [[Persona alloc] init];
    Persona* persona2= [[Persona alloc] init];
    Persona* persona3= [[Persona alloc] init];
    Persona* persona4= [[Persona alloc] init];
    Persona* persona5= [[Persona alloc] init];
    
    Persona* personaArray= [[Persona alloc] init];
    
    // Añadiendo valores a los atributos de cada objeto
    persona1.nombre=@"Javi ";
    persona1.apellido=@"Varas";
    persona1.telefono=@"632515425";
    
    persona2.nombre=@"Jonathan ";
    persona2.apellido=@"Viera";
    persona2.telefono=@"654123025";
    
    persona3.nombre=@"Vicente ";
    persona3.apellido=@"Gómez";
    persona3.telefono=@"635241041";
    
    persona4.nombre=@"David ";
    persona4.apellido=@"García";
    persona4.telefono=@"698541635";
    
    persona5.nombre=@"Kevin Prince ";
    persona5.apellido=@"Boateng";
    persona5.telefono=@"6456258741";
    
    //Almacenamiento de los objetos dentro del NSArray
    NSArray *arrayPersonas = @[persona1, persona2, persona3, persona4, persona5];
    
    for (int i=0; i<[arrayPersonas count]; i++){
    //for (NSnumber *numero in arrayPersonas){
        personaArray=arrayPersonas[i];
        NSString *string= [personaArray nombreCompleto: personaArray];
        NSLog(@"El nombre de la persona que esta en la posicion %d es %@",i, string);
    }
    
}



@end
