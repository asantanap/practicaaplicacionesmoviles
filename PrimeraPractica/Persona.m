//
//  Persona.m
//  PrimeraPractica
//
//  Created by ULPGC on 3/12/16.
//  Copyright © 2016 ULPGC. All rights reserved.
//

#import "Persona.h"

@implementation Persona

// Constructor
-(id) init {
    self= [super init];
    if(self!= nil){
        
    }
    return self;
}

// Ejercicio 2.2

/*!
 @description Este metodo devuelve el nombre y apellido en una sola cadena

 @param persona Se debe pasar un objeto de tipo persona
 @return Devuelve el nombre y apellido todo junto
 */
-(NSString*)nombreCompleto: (Persona *) persona{
    if (persona.nombre== NULL || persona.apellido==NULL){
        NSLog(@"No se ha encontrado ningun nombre o apellido");
    }
    NSString *nombreApellido =[persona.nombre stringByAppendingString: persona.apellido];
    return nombreApellido;
}

//Ejercicio 2.3

/*!
 @description Este metdoo devuelve si el nombre buscado coincide con el del objeto persona

 @param persona Se debe pasar un objeto de tipo persona
 @return Devuelve un booleano indicando si se ha encontrado o no
 */
-(BOOL)buscar:(Persona *) persona{
    NSString *nombreEncontrar =@"David";
    if (persona.nombre== NULL){
        NSLog(@"No se ha introducido el nombre");
    }
    BOOL encontrado= [persona.nombre isEqualToString: nombreEncontrar];
    return encontrado;   
};

@end
