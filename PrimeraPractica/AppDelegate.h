//
//  AppDelegate.h
//  PrimeraPractica
//
//  Created by ULPGC on 29/11/16.
//  Copyright © 2016 ULPGC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

