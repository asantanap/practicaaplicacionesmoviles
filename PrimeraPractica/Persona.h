//
//  Persona.h
//  PrimeraPractica
//
//  Created by ULPGC on 3/12/16.
//  Copyright © 2016 ULPGC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Persona : NSObject
//Ejercicio 2.1

@property(nonatomic, strong) NSString *nombre;

@property(nonatomic, strong) NSString *apellido;

@property(nonatomic, strong) NSString *telefono;

//Ejercico 2.2

-(NSString*)nombreCompleto: (Persona *) persona;

//Ejercico 2.3

-(BOOL)buscar:(Persona *)persona;

@end
